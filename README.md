Quiz Generator - Made with Symfony 2.6
================

[![Build Status](https://travis-ci.org/laravel/framework.svg)](https://travis-ci.org/chalasr/PHP_Avance_my_quiz)

This application was created by Robin CHALAS - FullStack Web Developer -  http://www.chalasdev.fr/

Problems? Issues?
--------------

Write a message on chalasdev.fr or create an issue

This application requires:
-------------

- PHP >= 5.4
- Composer

Getting Started
---------------

## Automatic ##

``` wget -O - https://raw.githubusercontent.com/chalasr/PHP_Avance_my_quiz/master/install.sh | sh```

## Manual ##

- Clone this repository

``` git clone https://github.com/chalasr/PHP_Avance_my_quiz.git ```

- Install vendor using composer

``` composer install ```

- Create database

``` doctrine:database:create ```

- Create schema of database

``` doctrine:schema:create ```

- Start server

``` php app/console server:run ```

Enjoy !

Credits
-------

Author : [Robin Chalas](http://www.chalasdev.fr/)

License
-------

Copyright (c) 2014-2015 [Robin Chalas](http://www.chaladev.fr/)
[License GPL V3](http://opensource.org/licenses/GPL-3.0) [![License](http://img.shields.io/:license-gpl3-blue.svg)](http://www.gnu.org/licenses/gpl-3.0.html)

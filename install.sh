#!/bin/sh

echo '[git] Downloading the project'
git clone https://github.com/chalasr/PHP_Avance_my_quiz.git
sudo chmod -R 777 PHP_Avance_my_quiz
cd PHP_Avance_my_quiz

echo '[curl] Getting Composer, the PHP dependency manager'
curl -sS https://getcomposer.org/installer | php
sudo chmod -R 777 *
echo '[composer] Downloading the dependencies'
php composer.phar install 

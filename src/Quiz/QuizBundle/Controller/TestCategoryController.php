<?php

namespace Quiz\QuizBundle\Controller;

use Quiz\QuizBundle\Entity\Category;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class TestCategoryController extends Controller
{
    // public function indexAction()
    // {
    //     $repository = $this->getDoctrine()
    //     ->getManager()
    //     ->getRepository('QuizQuizBundle:Category');
    //
    //     $listCategorys = $repository->findAll();
    //
    //     return $this->render('QuizQuizBundle:Category:index.html.twig', [
    //         'listCategorys' => $listCategorys,
    //     ]);
    // }

    public function viewAction($id)
    {
        $repository = $this->getDoctrine()
        ->getManager()
        ->getRepository('QuizQuizBundle:Category');

        $category = $repository->find($id);
        if (null === $category) {
            throw new NotFoundHttpException("Category ".$id." doesn't exist !");
        }

        return $this->render('QuizQuizBundle:Category:view.html.twig', [
            'category' => $category,
        ]);
    }

    public function newAction(Request $request)
    {
        $category = new Category();
        $form = $this->createForm(new Category(), $category);
        $form = $this->createFormBuilder($category)
        ->add('name', 'text')
        ->add('description', 'date')
        ->add('submit', 'submit')
        ->getForm();

        return $this->render('QuizQuizBundle:Category:new.html.twig', array(
            'form' => $form->createView(),
        ));
    }
}

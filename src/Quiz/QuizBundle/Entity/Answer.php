<?php

namespace Quiz\QuizBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Answer
 */
class Answer
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $content;

    /**
     * @var integer
     */
    private $good;

    public function __toString()
    {
        return $this->getName();

    }
    

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set content
     *
     * @param string $content
     * @return Answer
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set good
     *
     * @param integer $good
     * @return Answer
     */
    public function setGood($good)
    {
        $this->good = $good;

        return $this;
    }

    /**
     * Get good
     *
     * @return integer
     */
    public function getGood()
    {
        return $this->good;
    }
    /**
     * @var \Quiz\QuizBundle\Entity\Question
     */
    private $question;


    /**
     * Set question
     *
     * @param \Quiz\QuizBundle\Entity\Question $question
     * @return Answer
     */
    public function setQuestion(\Quiz\QuizBundle\Entity\Question $question = null)
    {
        $this->question = $question;

        return $this;
    }

    /**
     * Get question
     *
     * @return \Quiz\QuizBundle\Entity\Question
     */
    public function getQuestion()
    {
        return $this->question;
    }
}

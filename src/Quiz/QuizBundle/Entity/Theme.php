<?php

namespace Quiz\QuizBundle\Entity;


/**
 * Theme
 */
class Theme
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $description;



    public function __toString()
    {
        return $this->getName();

    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param  string $name
     * @return Theme
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param  string $description
     * @return Theme
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }
    /**
     * @var \Quiz\QuizBundle\Entity\Category
     */
    private $category;

    /**
     * Set category
     *
     * @param  \Quiz\QuizBundle\Entity\Category $category
     * @return Theme
     */
    public function setCategory(\Quiz\QuizBundle\Entity\Category $category = null)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category
     *
     * @return \Quiz\QuizBundle\Entity\Category
     */
    public function getCategory()
    {
        return $this->category;
    }
}

<?php

namespace Quiz\UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class DefaultController extends Controller
{
    /**
     * @Route("/hello/{name}")
     * @Template()
     */
     public function indexAction($name)
     {
         return $this->render('RobinUserBundle:Default:index.html.twig', array('name' => $name));
     }

     /**
     * @Route("/")
     * @Template()
     */
     public function homeAction($name)
     {
         return $this->render('RobinUserBundle:Default:home.html.twig');
     }
}
